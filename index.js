/*
    1. Declare 3 variables without initialization called username,password and role.
*/
let username;
let password;
let role;

/*
    
    2. Create a login function which is able to prompt the user to provide their username, password and role.
        -use prompt() and update the username,password and role global variable with the prompt() returned values.
        -add an if statement to check if the the username is an empty string or null or if the password is an empty string or null or if the role is an empty string or null.
            -if it is, show an alert to inform the user that their input should not be empty.
        -add an else statement. Inside the else statement add a switch to check the user's role add 3 cases and a default:
                -if the user's role is admin, show an alert with the following message:
                    "Welcome back to the class portal, admin!"
                -if the user's role is teacher, show an alert with the following message:
                    "Thank you for logging in, teacher!"
                -if the user's role is a rookie, show an alert with the following message:
                    "Welcome to the class portal, student!"
                -if the user's role does not fall under any of the cases, as a default, show a message:
                    "Role out of range."
*/
//
//
// Will be breaking up functions to keep each function short and easier to read.
const inputHander = (userNameInput, passwordInput, roleInput) => {
  username = userNameInput;
  password = passwordInput;
  role = roleInput;

  switch (role) {
    case "admin":
      alert("Welcome back to the class portal, admin!");
      break;
    case "teacher":
      alert("Thank you for logging in, teacher!");
      break;
    case "rookie":
      alert("Welcome to the class portal, student!");
      break;
    default:
      alert("Role out of range");
  }
};

const longinHandler = () => {
  const userNameInput = prompt("Enter Username:");
  const passwordInput = prompt("Enter Password:");
  const roleInput = prompt("Enter Role:");

  !userNameInput || !passwordInput || !roleInput
    ? alert("Please Complete Credentials.")
    : inputHander(userNameInput, passwordInput, roleInput);
};

longinHandler();

/*
    3. Create a function which is able to receive 4 numbers as arguments calculate its average and log a message for  the user about their letter equivalent in the console.
        -add parameters appropriate to describe the arguments.
        -create a new function scoped variable called average.
        -calculate the average of the 4 number inputs and store it in the variable average.
        -research the use of Math.round() and round off the value of the average variable.
            -update the average variable with the use of Math.round()
            -console.log() the average variable to check if it is rounding off first.
        -add an if statement to check if the value of average is less than or equal to 74.
            -if it is, show the following message in a console.log():
            "Hello, student, your average is <show average>. The letter equivalent is F"
        -add an else if statement to check if the value of average is greater than or equal to 75 and if average is less than or equal to 79.
            -if it is, show the following message in a console.log():
            "Hello, student, your average is <show average>. The letter equivalent is D"
        -add an else if statement to check if the value of average is greater than or equal to 80 and if average is less than or equal to 84.
            -if it is, show the following message in a console.log():
            "Hello, student, your average is <show average>. The letter equivalent is C"
        -add an else if statement to check if the value of average is greater than or equal to 85 and if average is less than or equal to 89.
            -if it is, show the following message in a console.log():
            "Hello, student, your average is <show average>. The letter equivalent is B"
        -add an else if statement to check if the value of average is greater than or equal to 90 and if average is less than or equal to 95.
            -if it is, show the following message in a console.log():
            "Hello, student, your average is <show average>. The letter equivalent is A"
        -add an else if statement to check if the value of average is greater than 96.
            -if it is, show the following message in a console.log():
            "Hello, student, your average is <show average>. The letter equivalent is A+"

        Invoke and add a number as argument using the browser console.

        Stretch goal:
            -Add an if statement to check the role of the user.
                -if the role of the user is currently "teacher" or "admin" or undefined or null, show an alert:
                "<role>! You are not allowed to access this feature!".
                -else, continue to evaluate the letter equivalent of the student's average.

*/

const letterEquivalentHandler = (grade) => {
  if (grade <= 74) {
    console.log(
      `Hello, student, your average is ${grade}. The letter equivalent is F.`
    );
  } else if (grade >= 75 && grade <= 79) {
    console.log(
      `Hello, student, your average is ${grade}. The letter equivalent is D.`
    );
  } else if (grade >= 80 && grade <= 84) {
    console.log(
      `Hello, student, your average is ${grade}. The letter equivalent is C.`
    );
  } else if (grade >= 85 && grade <= 89) {
    console.log(
      `Hello, student, your average is ${grade}. The letter equivalent is B.`
    );
  } else if (grade >= 90 && grade <= 95) {
    console.log(
      `Hello, student, your average is ${grade}. The letter equivalent is A.`
    );
  } else {
    console.log(
      `Hello, student, your average is ${grade}. The letter equivalent is A+.`
    );
  }
};

const averageHandler = (a, b, c, d) => {
  const average = (a + b + c + d) / 4;
  const roundedAverage = Math.round(average);
  letterEquivalentHandler(roundedAverage);
};

// --if credentials are not complete, will use the else-if
// --when role is out of range, will use the else
// --to be more specific, averageHandler() can only be invoked if value of role is rookie;
if (role === "rookie") {
  averageHandler(100, 89, 95, 90);
} else if (role === "teacher" || role === "admin" || !role) {
  console.log(`${role}! You are not allowed to access this feature!`);
} else {
  console.log("Invalid Role.");
}
